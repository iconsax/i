/*---- BACK TO TOP BUTTON ---*/
let btt = document.querySelector("[back-to-top] > button");
if(btt){
	window.addEventListener("scroll", (e) => {
		let scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
		if(scrollTop >= 500){
			btt.parentNode.classList.add("f");
		} else {
			btt.parentNode.classList.remove("f");
		}
	})
	
	btt.addEventListener("click", () => {
		document.documentElement.scrollTop = 0;
	})
}

/*---- FUNCTION THAT ASSIGNS ICONS TO CATEGORIES ---*/
function setCat(iconName, iconCat){
	let targetIcon = document.querySelector(`.icon-each[icon-name="${iconName}"]`);
	let targetArea = document.querySelector(`h2#${iconCat} + .grid`);
	if(targetIcon && targetArea){
		let clone = targetIcon.cloneNode(true);
		targetArea.append(clone)
		
		//targetArea.append(targetIcon)
	} else {
		console.info(`Couldn't find [${iconName}]`)
	}
}

/*---- GENERATE TABLE OF CONTENTS ---*/
let tableOfContents = document.querySelector(".table-of-contents");
if(tableOfContents){
	let ul = document.createElement("ul");
	tableOfContents.append(ul);
	
	document.querySelectorAll("section h2[id]:not([id='']):not(#how-to-use)")?.forEach(h2 => {
		let h2_shown = h2.textContent;
		let h2_val = h2.id;
		
		let li = document.createElement("li");
		ul.append(li);
		
		let a = document.createElement("a");
		a.textContent = h2_shown;
		a.href = `#${h2_val}`;
		li.append(a)
	})
}

fetch("https://iconsax.gitlab.io/i/icons-list.html")
.then(response => response.text())
.then(contents => {
    //console.log(contents);
	
	let maingrid = document.querySelector(".main-grid");
	if(maingrid){
		maingrid.innerHTML = contents;
		
		setTimeout(() => {
			maingrid.querySelectorAll(":scope > .iconsax")?.forEach(i => {
				if(!i.parentNode.matches(".icon-each")){
					let e = document.createElement("div");
					e.classList.add("icon-each");
					i.before(e);
					e.append(i);
					
					let iconName = i.getAttribute("icon-name");
					if(iconName.trim() !== ""){
						e.setAttribute("icon-name",iconName)

						let span = document.createElement('span');
						span.textContent = iconName;
						span.classList.add("icon-label");
						e.append(span);
					}//end: icon name
				}//end: if no parent wrap
			})//end iconsax forEach
			
			setCat("add","common-ui");
			setCat("add-circle","common-ui");
			setCat("add-square","common-ui");
			setCat("minus","common-ui");
			setCat("minus-circle","common-ui");
			setCat("minus-square","common-ui");
			setCat("x","common-ui");
			setCat("x-circle","common-ui");
			setCat("x-square","common-ui");
			setCat("circle","common-ui");
			setCat("tick-circle","common-ui");
			setCat("tick-square","common-ui");
			setCat("mail","common-ui");
			setCat("home-1","common-ui");
			setCat("home-2","common-ui");
			setCat("reblog","common-ui");
			setCat("shuffle-1","common-ui");
			setCat("heart","common-ui");
			setCat("like","common-ui");
			setCat("dislike","common-ui");
			setCat("eye","common-ui");
			setCat("eye-slash","common-ui");
			setCat("link-1","common-ui");
			setCat("link-2","common-ui");
			setCat("link-3","common-ui");
			setCat("link-4","common-ui");
			setCat("link-circle","common-ui");
			setCat("link-square","common-ui");
			setCat("external-circle","common-ui");
			setCat("external-square","common-ui");
			setCat("book-open","common-ui");
			setCat("bell-1","common-ui");
			setCat("bell-2","common-ui");
			setCat("search-normal-2","common-ui");
			setCat("search-zoom-in","common-ui");
			setCat("search-zoom-out-2","common-ui");
			setCat("tag-1","common-ui");
			setCat("tag-2","common-ui");
			setCat("hashtag","common-ui");
			setCat("sun","common-ui");
			setCat("bookmarks","common-ui");
			setCat("user-1","common-ui");
			setCat("user-2","common-ui");
			setCat("edit-1","common-ui");
			setCat("edit-2","common-ui");
			setCat("globe","common-ui");
			setCat("star","common-ui");
			setCat("star-slash","common-ui");
			setCat("play","common-ui");			
			setCat("pause","common-ui");			
			setCat("menu-meatballs","common-ui");
			setCat("hamburger-menu","common-ui");
			setCat("notepad","common-ui");
			setCat("stickynote","common-ui");
			setCat("grid-apps","common-ui");
			setCat("verify","common-ui");
			setCat("send-1","common-ui");
			setCat("send-2","common-ui");
			setCat("trash","common-ui");
			setCat("trash-square","common-ui");
			
			setCat("arrow-up","arrows-style-1");
			setCat("arrow-down","arrows-style-1");
			setCat("arrow-left","arrows-style-1");
			setCat("arrow-right","arrows-style-1");
			
			setCat("arrow-up-circle","arrows-style-2");
			setCat("arrow-down-circle","arrows-style-2");
			setCat("arrow-left-circle","arrows-style-2");
			setCat("arrow-right-circle","arrows-style-2");
			
			setCat("chevron-up","arrows-style-3");
			setCat("chevron-down","arrows-style-3");
			setCat("chevron-left","arrows-style-3");
			setCat("chevron-right","arrows-style-3");
			
			setCat("chevron-up-circle","arrows-style-4");
			setCat("chevron-down-circle","arrows-style-4");
			setCat("chevron-left-circle","arrows-style-4");
			setCat("chevron-right-circle","arrows-style-4");
			
			setCat("chevron-up-square","arrows-style-5");
			setCat("chevron-down-square","arrows-style-5");
			setCat("chevron-left-square","arrows-style-5");
			setCat("chevron-right-square","arrows-style-5");
			
			setCat("arrow-up-thick-1","arrows-style-6");
			setCat("arrow-down-thick-1","arrows-style-6");
			setCat("arrow-left-thick-1","arrows-style-6");
			setCat("arrow-right-thick-1","arrows-style-6");
			
			setCat("arrow-up-thick-3","arrows-style-7");
			setCat("arrow-down-thick-3","arrows-style-7");
			setCat("arrow-left-thick-3","arrows-style-7");
			setCat("arrow-right-thick-3","arrows-style-7");
			
			setCat("arrow-right-thick-2","arrows-misc");
			setCat("arrow-up-down","arrows-misc");
			setCat("convert","arrows-misc");
			setCat("directions","arrows-misc");
			setCat("directions-square","arrows-misc");
			setCat("history","arrows-misc");
			setCat("shuffle-2","arrows-misc");
			setCat("swap-horizontal","arrows-misc");
			setCat("swap-horizontal-circle","arrows-misc");
			setCat("swap-horizontal-square","arrows-misc");
			setCat("swap-vertical","arrows-misc");
			setCat("swap-vertical-circle","arrows-misc");
			setCat("swap-vertical-square","arrows-misc");
			setCat("redo","arrows-misc");
			setCat("redo-square","arrows-misc");
			setCat("refresh","arrows-misc");
			setCat("refresh-circle","arrows-misc");
			setCat("refresh-left-square","arrows-misc");
			setCat("refresh-right-square","arrows-misc");
			setCat("refresh-square","arrows-misc");
			setCat("repeat","arrows-misc");
			setCat("repeat-circle","arrows-misc");
			setCat("retweet","arrows-misc");
			setCat("rotate-left","arrows-misc");
			setCat("rotate-right","arrows-misc");
			setCat("send-diagonal-square","arrows-misc");
			setCat("send-diagonal-up","arrows-misc");
			setCat("undo","arrows-misc");
			setCat("undo-square","arrows-misc");
			
			setCat("24hr-service","date-time");
			setCat("calendar-1","date-time");
			setCat("calendar-2","date-time");
			setCat("calendar-3","date-time");
			setCat("calendar-circle","date-time");
			setCat("calendar-add","date-time");
			setCat("calendar-edit","date-time");
			setCat("calendar-search","date-time");
			setCat("calendar-tick","date-time");
			setCat("calendar-x","date-time");
			setCat("clock","date-time");
			setCat("hourglass","date-time");
			setCat("infinite","date-time");
			setCat("stopwatch","date-time");
			setCat("stopwatch-play","date-time");
			setCat("stopwatch-pause","date-time");
			setCat("watch-1","date-time");
			setCat("watch-2","date-time");
			setCat("watch-activity","date-time");
			
			setCat("globe","location");
			setCat("globe-edit","location");
			setCat("globe-refresh","location");
			setCat("globe-search","location");
			setCat("map-1","location");
			setCat("map-2","location");
			setCat("map-3","location");
			setCat("signpost","location");
			setCat("directions","location");
			setCat("directions-square","location");
			setCat("compass-1","location");
			setCat("compass-2","location");
			setCat("gps","location");
			setCat("gps-slash","location");
			setCat("location","location");
			setCat("location-add","location");
			setCat("location-minus","location");
			setCat("location-slash","location");
			setCat("location-tick","location");
			setCat("location-x","location");
			setCat("route-1","location");
			setCat("route-2","location");
			setCat("cursor-square-1","location");
			setCat("radar-1","location");
			setCat("radar-2","location");
			
			setCat("home-1","buildings");
			setCat("home-2","buildings");
			setCat("bank","buildings");
			setCat("building-1","buildings");
			setCat("building-2","buildings");
			setCat("building-3","buildings");
			setCat("building-4","buildings");
			setCat("buildings-1","buildings");
			setCat("buildings-2","buildings");
			setCat("house-1","buildings");
			setCat("house-2","buildings");
			setCat("shop","buildings");
			setCat("hospital","buildings");
			setCat("courthouse","buildings");
			
			setCat("devices-1","tech");
			setCat("devices-2","tech");
			setCat("mobile","tech");
			setCat("auto-brightness","tech");
			setCat("phone","tech");
			setCat("airdrop","tech");
			setCat("airpod","tech");
			setCat("airpods","tech");
			setCat("phone-add","tech");
			setCat("phone-minus","tech");
			setCat("phone-outgoing","tech");
			setCat("phone-receive","tech");
			setCat("phone-ringing","tech");
			setCat("phone-slash","tech");
			setCat("phone-tick","tech");
			setCat("phone-x","tech");
			setCat("monitor","tech");
			setCat("monitor-message","tech");
			setCat("monitor-record","tech");
			setCat("watch-1","tech");
			setCat("watch-2","tech");
			setCat("watch-activity","tech");
			setCat("watch-1","tech");
			setCat("radio","tech");
			setCat("ram-1","tech");
			setCat("ram-2","tech");
			setCat("simcard-1","tech");
			setCat("simcard-2","tech");
			setCat("simcards","tech");
			setCat("smart-home","tech");
			setCat("speaker","tech");
			setCat("speedometer","tech");
			setCat("radar-3","tech");
			setCat("wifi","tech");
			setCat("wifi-square","tech");
			setCat("home-wifi","tech");
			setCat("battery-disable","tech");
			setCat("battery-empty","tech");
			setCat("battery-1","tech");
			setCat("battery-2","tech");
			setCat("battery-full","tech");
			setCat("battery-charging","tech");
			setCat("bluetooth","tech");
			setCat("bluetooth-2","tech");
			setCat("bluetooth-circle","tech");
			setCat("bluetooth-rectangle","tech");
			setCat("fingerprint-scan","tech");
			setCat("scan-1","tech");
			setCat("scan-2","tech");
			setCat("scan-3","tech");
			setCat("scan-4","tech");
			setCat("calculator","tech");
			setCat("camera","tech");
			setCat("camera-slash","tech");
			setCat("cast","tech");
			setCat("cd","tech");
			setCat("cloud-change","tech");
			setCat("cloud-connection","tech");
			setCat("code-1","tech");
			setCat("code-2","tech");
			setCat("code-circle","tech");
			setCat("cpu","tech");
			setCat("cpu-charge","tech");
			setCat("cpu-setting","tech");
			setCat("driver-1","tech");
			setCat("driver-2","tech");
			setCat("driver-refresh","tech");
			setCat("external-drive","tech");
			setCat("mic-1","tech");
			setCat("mic-2","tech");
			setCat("mic-slash-1","tech");
			setCat("mic-slash-2","tech");
			setCat("mouse","tech");
			setCat("printer","tech");
			setCat("printer-slash","tech");
			setCat("telescope","tech");
			setCat("telescope","tech");			
			setCat("tuning-knob","tech");
			setCat("qr-code","tech");
			setCat("plug","tech");
			setCat("bulb","tech");			
			setCat("bulb-charge","tech");
			setCat("bulb-slash","tech");
			
			setCat("watch-activity","health");
			setCat("heart-monitor-square","health");
			setCat("weights","health");
			setCat("weight-scale","health");
			
			setCat("award","gaming");
			setCat("award-2","gaming");
			setCat("award-3","gaming");
			setCat("gameboy","gaming");
			setCat("game-controller","gaming");
			setCat("headphones","gaming");
			setCat("keyboard-1","gaming");
			setCat("ranking","gaming");
			setCat("ranking-1","gaming");
			setCat("trophy","gaming");
			
			setCat("cloud","nature");
			setCat("cloud-tick","nature");
			setCat("cloud-x-1","nature");
			setCat("cloud-x-circle","nature");
			setCat("cloud-fog","nature");
			setCat("cloud-lightning","nature");
			setCat("cloud-minus","nature");
			setCat("cloud-notif","nature");
			setCat("cloud-add","nature");
			setCat("cloud-rain","nature");
			setCat("cloud-snow","nature");
			setCat("cloud-sunny","nature");
			setCat("drop","nature");
			setCat("moon","nature");
			setCat("snow","nature");
			setCat("sun","nature");
			setCat("sun-fog","nature");
			setCat("wind-1","nature");
			setCat("wind-2","nature");
			setCat("tree","nature");
			setCat("star","nature");
			setCat("star-speed","nature");
			setCat("ranking-1","nature");
			setCat("flash-1","nature");
			
			setCat("24hr-service","ecommerce");
			setCat("add","ecommerce");
			setCat("add-circle","ecommerce");
			setCat("add-square","ecommerce");
			setCat("bank","ecommerce");
			setCat("bank-card","ecommerce");
			setCat("bank-card-add","ecommerce");
			setCat("bank-card-convert","ecommerce");
			setCat("bank-card-diagonal","ecommerce");
			setCat("bank-card-receive","ecommerce");
			setCat("bank-card-send","ecommerce");
			setCat("bank-card-slash","ecommerce");
			setCat("bank-card-tick-1","ecommerce");
			setCat("bank-card-tick-2","ecommerce");
			setCat("bank-card-x-1","ecommerce");
			setCat("bank-card-x-2","ecommerce");
			setCat("bank-cards","ecommerce");
			setCat("barcode","ecommerce");
			setCat("basket-1","ecommerce");
			setCat("basket-2","ecommerce");
			setCat("basket-3","ecommerce");
			setCat("basket-happy","ecommerce");
			setCat("basket-tick-1","ecommerce");
			setCat("basket-tick-2","ecommerce");
			setCat("basket-time","ecommerce");
			setCat("basket-x","ecommerce");
			setCat("basket-x-1","ecommerce");
			setCat("bill","ecommerce");
			setCat("box","ecommerce");
			setCat("box-add","ecommerce");
			setCat("box-dashed","ecommerce");
			setCat("box-rotate","ecommerce");
			setCat("box-scan","ecommerce");
			setCat("box-search","ecommerce");
			setCat("box-square","ecommerce");
			setCat("box-swap","ecommerce");
			setCat("box-tick","ecommerce");
			setCat("box-time","ecommerce");
			setCat("box-x","ecommerce");
			setCat("card-coin","ecommerce");
			setCat("card-edit","ecommerce");
			setCat("coins-2","ecommerce");
			setCat("coins-3","ecommerce");
			setCat("coins-4","ecommerce");
			setCat("discount-badge","ecommerce");
			setCat("discount-circle","ecommerce");
			setCat("dollar-circle","ecommerce");
			setCat("dollar-square","ecommerce");
			setCat("gift","ecommerce");
			setCat("money-1","ecommerce");
			setCat("money-2","ecommerce");
			setCat("money-3","ecommerce");
			setCat("money-4","ecommerce");
			setCat("money-5","ecommerce");
			setCat("money-add","ecommerce");
			setCat("money-change","ecommerce");
			setCat("money-forbidden","ecommerce");
			setCat("money-in","ecommerce");
			setCat("money-out","ecommerce");
			setCat("money-tick","ecommerce");
			setCat("money-time","ecommerce");
			setCat("money-x","ecommerce");
			setCat("package","ecommerce");
			setCat("percentage-circle","ecommerce");
			setCat("percentage-square","ecommerce");
			setCat("receipt","ecommerce");
			setCat("receipt-2","ecommerce");
			setCat("receipt-3","ecommerce");
			setCat("receipt-4","ecommerce");
			setCat("receipt-add","ecommerce");
			setCat("receipt-discount-1","ecommerce");
			setCat("receipt-discount-2","ecommerce");
			setCat("receipt-edit","ecommerce");
			setCat("receipt-list","ecommerce");
			setCat("receipt-minus-1","ecommerce");
			setCat("receipt-minus-2","ecommerce");
			setCat("receipt-search","ecommerce");
			setCat("receipt-square","ecommerce");
			setCat("receipt-text","ecommerce");
			setCat("shield-card","ecommerce");
			setCat("shop","ecommerce");
			setCat("shop-add","ecommerce");
			setCat("shop-minus","ecommerce");
			setCat("shopping-cart","ecommerce");
			setCat("tag-1","ecommerce");
			setCat("tag-2","ecommerce");
			setCat("ticket-1","ecommerce");
			setCat("ticket-2","ecommerce");
			setCat("ticket-discount","ecommerce");
			setCat("ticket-star","ecommerce");
			setCat("ticket-tear","ecommerce");
			setCat("truck","ecommerce");
			setCat("truck-speed","ecommerce");
			setCat("truck-tick","ecommerce");
			setCat("truck-time","ecommerce");
			setCat("truck-x","ecommerce");
			setCat("wallet-1","ecommerce");
			setCat("wallet-2","ecommerce");
			setCat("wallet-3","ecommerce");
			setCat("wallet-4","ecommerce");
			setCat("wallet-add","ecommerce");
			setCat("wallet-add-1","ecommerce");
			setCat("wallet-minus","ecommerce");
			setCat("wallet-money","ecommerce");
			setCat("wallet-open","ecommerce");
			setCat("wallet-open-add","ecommerce");
			setCat("wallet-open-change","ecommerce");
			setCat("wallet-open-tick","ecommerce");
			setCat("wallet-open-time","ecommerce");
			setCat("wallet-open-x","ecommerce");
			setCat("wallet-search","ecommerce");
			setCat("wallet-tick","ecommerce");
			setCat("wallet-x","ecommerce");
			
			setCat("24hr-service","customer-service");
			setCat("bell-3","customer-service");
			setCat("infinite","customer-service");
			setCat("lifebuoy","customer-service");
			setCat("question-message","customer-service");
			setCat("emoji-happy","customer-service");
			setCat("emoji-sad","customer-service");
			setCat("emoji-normal","customer-service");
			setCat("like","customer-service");
			setCat("dislike","customer-service");
			setCat("like-dislike","customer-service");
			setCat("like-badge","customer-service");
			setCat("like-tag","customer-service");
			setCat("smileys","customer-service");
			
			setCat("alarm","security");
			setCat("home-shield","security");
			setCat("key","security");
			setCat("key-square","security");
			setCat("lock-1","security");
			setCat("lock-2","security");
			setCat("lock-circle","security");
			setCat("lock-slash","security");
			setCat("password-check","security");
			setCat("safebox-1","security");
			setCat("safebox-2","security");
			setCat("scan-1","security");
			setCat("scan-2","security");
			setCat("scan-3","security");
			setCat("shield","security");
			setCat("shield-card","security");
			setCat("shield-lock","security");
			setCat("shield-search","security");
			setCat("shield-slash","security");
			setCat("shield-tick","security");
			setCat("shield-time","security");
			setCat("shield-user","security");
			setCat("shield-x","security");
			setCat("unlock","security");			
			
			setCat("alarm","warnings");
			setCat("bell-1","warnings");
			setCat("bell-2","warnings");
			setCat("bell-3","warnings");
			setCat("warning-triangle","warnings");
			setCat("warning-octagon","warnings");
			setCat("not-allowed-1","warnings");
			setCat("not-allowed-2","warnings");
			setCat("not-allowed-3","warnings");
			setCat("info-badge","warnings");
			setCat("info-circle","warnings");
			
			setCat("cake","food");
			setCat("coffee","food");
			setCat("milk","food");
			
			setCat("airplane","transport");
			setCat("airplane-square","transport");
			setCat("bus","transport");
			setCat("car","transport");
			setCat("smart-car","transport");
			setCat("driving","transport");
			setCat("gas-station","transport");
			setCat("ship","transport");
			setCat("truck","transport");
			
			setCat("text","text");
			setCat("uppercase-lowercase","text");
			setCat("bold","text");
			setCat("italic","text");
			setCat("underline","text");
			setCat("backspace","text");
			setCat("clipboard","text");
			setCat("clipboard-in","text");
			setCat("clipboard-out","text");
			setCat("clipboard-text-1","text");
			setCat("clipboard-text-2","text");
			setCat("clipboard-tick","text");
			setCat("clipboard-x","text");
			setCat("code-clipboard","text");
			setCat("copy","text");
			setCat("copy-tick","text");
			setCat("document-copy","text");
			setCat("edit-1","text");
			setCat("edit-2","text");
			setCat("eraser","text");
			setCat("eraser-square","text");
			setCat("filter","text");
			setCat("first-character","text");
			setCat("keyboard-1","text");
			setCat("keyboard-2","text");
			setCat("language-circle","text");
			setCat("language-square","text");
			setCat("line-spacing","text");
			setCat("link-1","text");
			setCat("link-2","text");
			setCat("link-3","text");
			setCat("link-4","text");
			setCat("paperclip-1","text");
			setCat("paperclip-2","text");
			setCat("printer","text");
			setCat("printer-slash","text");
			
			setCat("quote-start","text");
			setCat("quote-start-circle","text");
			setCat("quote-start-square","text");
			
			setCat("quote-end","text");			
			setCat("quote-end-circle","text");
			setCat("quote-end-square","text");
			
			setCat("undo","text");
			setCat("undo-square","text");
			setCat("redo","text");
			setCat("redo-square","text");
			setCat("recover","text");
			setCat("ruler","text");
			setCat("ruler-and-pen","text");
			setCat("sort","text");
			setCat("task-list","text");
			setCat("task-list-square","text");
			setCat("text-align-center","text");
			setCat("text-align-justify-center","text");
			setCat("text-align-justify-left","text");
			setCat("text-align-justify-right","text");
			setCat("text-align-left","text");
			setCat("text-align-right","text");
			setCat("text-block","text");
			setCat("translate","text");
			
			setCat("document-1","document");
			setCat("document-2","document");
			setCat("document-cloud","document");
			setCat("document-code-1","document");
			setCat("document-code-2","document");
			setCat("document-copy","document");
			setCat("document-download","document");
			setCat("document-favorite","document");
			setCat("document-favorite-1","document");
			setCat("document-filter","document");
			setCat("document-forward","document");
			setCat("document-previous","document");
			setCat("document-text-1","document");
			setCat("document-text-2","document");
			setCat("document-upload","document");
			
			setCat("archive-book","note");
			setCat("archive-closed","note");
			setCat("archive-open","note");
			setCat("book-closed","note");
			setCat("book-open","note");
			setCat("book-with-bookmark","note");
			setCat("book-square","note");
			setCat("bookmark-add","note");
			setCat("bookmark-minus","note");
			setCat("bookmark-minus-2","note");
			setCat("bookmark-slash","note");
			setCat("bookmark-tick","note");
			setCat("bookmarks","note");
			setCat("bookmarks-add","note");
			setCat("bookmarks-minus","note");
			setCat("bookmarks-x","note");
			setCat("cue-cards","note");
			setCat("document-1","note");
			setCat("document-2","note");
			setCat("document-text-1","note");
			setCat("document-text-2","note");
			setCat("note","note");
			setCat("note-add","note");
			setCat("note-favorite","note");
			setCat("note-text","note");
			setCat("note-x","note");
			setCat("notepad","note");
			setCat("notes-1","note");
			setCat("notes-2","note");
			setCat("page-with-bookmark-1","note");
			setCat("page-with-bookmark-2","note");
			setCat("paperclip-1","note");
			setCat("paperclip-2","note");
			setCat("paperclip-circle","note");
			setCat("paperclip-square","note");
			setCat("stickynote","note");
			setCat("stickynote-round","note");
			setCat("task-list","note");
			setCat("task-list-square","note");
			
			setCat("menu-meatballs","menu");
			setCat("more-circle","menu");
			setCat("more-square","menu");
			setCat("hamburger-menu","menu");
			setCat("menu-4-dots","menu");
			setCat("menu-meatballs","menu");
			setCat("sort","menu");
			setCat("grid-apps","menu");
			setCat("grid-apps-2","menu");
			setCat("grid-apps-add","menu");
			setCat("grid-apps-equals","menu");
			setCat("media-sliders-1","menu");
			setCat("media-sliders-2","menu");
			setCat("media-sliders-3","menu");
			setCat("nut","menu");
			setCat("setting-1","menu");
			setCat("setting-2","menu");
			setCat("setting-3","menu");
			setCat("therefore","menu");
			
			setCat("picture","img");
			setCat("picture-1","img");
			setCat("picture-add","img");
			setCat("picture-download","img");
			setCat("picture-edit","img");
			setCat("picture-favorite","img");
			setCat("picture-slash","img");
			setCat("picture-tick","img");
			setCat("picture-upload","img");
			setCat("picture-x","img");
			
			setCat("3d-cube","design");
			setCat("add-layer","design");
			setCat("align-top","design");
			setCat("align-bottom","design");
			setCat("align-left","design");
			setCat("align-right","design");
			setCat("align-x-center","design");
			setCat("align-y-center","design");
			setCat("background-layer","design");
			setCat("foreground-layer","design");
			setCat("layers-1","design");
			setCat("layers-2","design");
			setCat("bezier","design");
			setCat("blend-1","design");
			setCat("blend-2","design");
			setCat("blur","design");
			setCat("bounding-box","design");
			setCat("bounding-circle","design");
			setCat("brush-1","design");
			setCat("brush-2","design");
			setCat("brush-3","design");
			setCat("brush-4","design");
			setCat("brush-5","design");
			setCat("brush-tools","design");
			setCat("change-shape-1","design");
			setCat("change-shape-2","design");
			setCat("color-filter","design");
			setCat("color-filter-square","design");
			setCat("color-swatch","design");
			setCat("component","design");
			setCat("crop","design");
			setCat("document-sketch","design");
			setCat("item-rotate-left","design");
			setCat("item-rotate-right","design");
			setCat("magic-star","design");
			setCat("magic-wand","design");
			setCat("main-component","design");
			setCat("mask","design");
			setCat("mask-1","design");
			setCat("mask-2","design");
			setCat("mask-3","design");
			setCat("paintbucket-1","design");
			setCat("paintbucket-2","design");
			setCat("paintbucket-circle","design");
			setCat("paintbucket-square","design");
			setCat("pen-tool-1","design");
			setCat("pen-tool-2","design");
			setCat("pen-path-1","design");
			setCat("pen-path-2","design");
			setCat("pen-tool-add","design");
			setCat("pen-tool-minus","design");
			setCat("pen-tool-square","design");
			setCat("pen-tool-x","design");
			setCat("shapes-1","design");
			setCat("shapes-2","design");
			setCat("size","design");
			
			setCat("video","video");
			setCat("video-2","video");
			setCat("video-add","video");
			setCat("video-slash","video");
			setCat("video-tick","video");
			setCat("video-x","video");
			setCat("video-time","video");
			setCat("video-horizontal","video");
			setCat("video-vertical","video");
			
			setCat("airpod","music");
			setCat("airpods","music");
			setCat("cd","music");
			setCat("earphones","music");
			setCat("headphones","music");
			setCat("headphones-active","music");
			setCat("radio","music");
			setCat("music","music");
			setCat("music-circle-dashed","music");
			setCat("music-dashboard","music");
			setCat("music-list","music");
			setCat("music-multi","music");
			setCat("music-playlist","music");
			setCat("music-square-1","music");
			setCat("music-square-2","music");
			setCat("music-square-3","music");
			setCat("music-square-4","music");
			setCat("music-square-add","music");
			setCat("music-square-search","music");
			setCat("music-square-x","music");
			setCat("musicnote","music");
			
			setCat("play","media");
			setCat("play-circle","media");
			setCat("play-circle-add","media");
			setCat("play-circle-x","media");
			setCat("play-circle-dashed","media");
			setCat("play-circle-x","media");
			setCat("play-square","media");
			setCat("play-octagon","media");			
			setCat("pause","media");
			setCat("pause-circle","media");			
			setCat("loop","media");			
			setCat("square","media");
			setCat("stop-circle","media");
			setCat("maximize","media");
			setCat("maximize-1","media");
			setCat("maximize-2","media");
			setCat("maximize-3","media");
			setCat("maximize-3-square","media");
			setCat("maximize-circle","media");
			setCat("media-backward","media");
			setCat("media-forward","media");
			setCat("media-backward-15s","media");
			setCat("media-backward-10s","media");
			setCat("media-backward-5s","media");
			setCat("media-forward-5s","media");
			setCat("media-forward-10s","media");
			setCat("media-forward-15s","media");
			setCat("media-previous","media");
			setCat("media-next","media");
			setCat("media-repeat","media");
			setCat("media-repeat-single","media");
			setCat("media-sliders-1","media");
			setCat("media-sliders-2","media");
			setCat("media-sliders-3","media");
			setCat("record-circle","media");
			setCat("setting-2","media");
			setCat("setting-3","media");
			setCat("mic-1","media");
			setCat("mic-2","media");
			setCat("mic-slash-1","media");
			setCat("mic-slash-2","media");
			setCat("pop-in-circle","media");
			setCat("pop-in-square","media");
			setCat("pop-out","media");
			setCat("pop-out-player","media");
			setCat("repeat","media");
			setCat("repeat-circle","media");
			setCat("shuffle-1","media");
			setCat("shuffle-2","media");
			setCat("slider","media");
			setCat("slideshow-horizontal-1","media");
			setCat("slideshow-horizontal-2","media");
			setCat("slideshow-vertical-1","media");
			setCat("slideshow-vertical-2","media");
			setCat("sound","media");
			setCat("sound-circle","media");
			setCat("subtitles","media");
			setCat("tuning-knob","media");
			setCat("voice-square","media");
			setCat("volume-slash","media");
			setCat("volume-x","media");
			setCat("volume-mute","media");
			setCat("volume-low","media");
			setCat("volume-high","media");
			setCat("volume-minus","media");
			setCat("volume-add","media");
			
			
			setCat("filter","filter");
			setCat("document-filter","filter");
			setCat("filter-add","filter");
			setCat("filter-edit","filter");
			setCat("filter-search","filter");
			setCat("filter-tick","filter");
			setCat("filter-x","filter");
			setCat("filter-square","filter");
			setCat("sort","filter");
			
			setCat("activity-chart","stats");
			setCat("activity-chart-favorite","stats");
			setCat("activity-square","stats");
			setCat("bar-graph-1","stats");
			setCat("bar-graph-2","stats");
			setCat("bar-graph-3","stats");
			setCat("bar-graph-4","stats");
			setCat("bar-graph-5","stats");
			setCat("briefcase","stats");
			setCat("briefcase-tick","stats");
			setCat("briefcase-time","stats");
			setCat("briefcase-x","stats");
			setCat("chart-square","stats");
			setCat("chart-tick","stats");
			setCat("chart-x","stats");
			setCat("copyright","stats");
			setCat("creative-commons","stats");
			setCat("flow-chart-1","stats");
			setCat("flow-chart-2","stats");
			setCat("hashtag-up","stats");
			setCat("hashtag-down","stats");
			setCat("home-trend-up","stats");
			setCat("home-trend-down","stats");
			setCat("kanban","stats");
			setCat("pie-chart","stats");
			setCat("presentation-chart","stats");
			setCat("ranking","stats");
			setCat("radial-chart","stats");
			setCat("trend-up","stats");
			setCat("trend-down-square","stats");
			
			setCat("code-1","coding");
			setCat("code-2","coding");
			setCat("code-circle","coding");
			setCat("code-clipboard","coding");
			setCat("code-tag","coding");
			setCat("command","coding");
			setCat("command-square","coding");
			setCat("cursor-circle","coding");
			setCat("cursor-circle-1","coding");
			setCat("cursor-square","coding");
			setCat("cursor-square-1","coding");
			setCat("document-code-1","coding");
			setCat("document-code-2","coding");
			setCat("git-arrows","coding");
			setCat("git-commit","coding");
			setCat("git-pull-request","coding");
			setCat("git-pull-request-square","coding");
			setCat("hierarchy-1","coding");
			setCat("hierarchy-1-square","coding");
			setCat("hierarchy-2","coding");
			setCat("hierarchy-2-square","coding");
			setCat("hierarchy-3","coding");
			setCat("scroll-horizontal-square","coding");
			
			setCat("user-1","user");
			setCat("user-1-add","user");
			setCat("user-1-minus","user");
			setCat("user-1-tick","user");
			setCat("user-1-x","user");
			setCat("user-1-square","user");
			setCat("user-1-tag","user");
			
			setCat("user-2","user");
			setCat("user-2-add","user");
			setCat("user-2-minus","user");
			setCat("user-2-tick","user");			
			setCat("user-2-x","user");
			setCat("user-2-tag","user");
			
			setCat("user-2-circle","user");
			setCat("user-2-circle-add","user");
			setCat("user-2-edit","user");
			setCat("user-2-search","user");
			setCat("user-octagon","user");
			setCat("users","user");
			setCat("group","user");
			setCat("person-card","user");
			setCat("female","user");
			setCat("male","user");
			
			setCat("align-top","layout");
			setCat("align-bottom","layout");
			setCat("align-left","layout");
			setCat("align-right","layout");
			setCat("align-x-center","layout");
			setCat("align-y-center","layout");
			setCat("rows-1","layout");
			setCat("rows-2","layout");
			setCat("columns","layout");
			setCat("kanban","layout");
			setCat("grid-apps","layout");
			setCat("grid-apps-2","layout");
			setCat("grid-apps-add","layout");
			setCat("grid-apps-equals","layout");
			setCat("grid-lock","layout");
			setCat("grid-quadrants","layout");
			setCat("grid-table","layout");
			setCat("grid-table-edit","layout");
			setCat("grid-table-eraser","layout");
			setCat("grid-uneven","layout");
			setCat("header","layout");
			setCat("footer","layout");
			setCat("sidebar-left","layout");
			setCat("sidebar-right","layout");
			setCat("layout-1","layout");
			setCat("layout-2","layout");
			setCat("layout-3","layout");
			setCat("layout-4","layout");
			setCat("layout-5","layout");
			setCat("layout-6","layout");
			setCat("layout-7","layout");
			setCat("layout-8","layout");
			setCat("layout-half","layout");
			setCat("slideshow-horizontal-1","layout");
			setCat("slideshow-horizontal-2","layout");
			setCat("slideshow-vertical-1","layout");
			setCat("slideshow-vertical-2","layout");
			setCat("text-align-center","layout");
			setCat("text-align-justify-center","layout");
			setCat("text-align-justify-left","layout");
			setCat("text-align-justify-right","layout");
			setCat("text-align-left","layout");
			setCat("text-align-right","layout");
			
			setCat("mail","msg");
			setCat("mail-edit","msg");
			setCat("mail-notif","msg");
			setCat("mail-search","msg");
			setCat("mail-speed","msg");
			setCat("mail-star","msg");
			setCat("message-add","msg");
			setCat("message-circle","msg");
			setCat("message-dash-1","msg");
			setCat("message-dash-2","msg");
			setCat("message-dash-add","msg");
			setCat("message-dash-tick","msg");
			setCat("message-dash-time","msg");
			setCat("message-dash-x","msg");
			setCat("message-dots","msg");
			setCat("message-dots-favorite","msg");
			setCat("message-edit","msg");
			setCat("message-minus","msg");
			setCat("message-notif","msg");
			setCat("message-search","msg");
			setCat("message-square","msg");
			setCat("message-text","msg");
			setCat("messages-1","msg");
			setCat("messages-2","msg");
			setCat("messages-3","msg");
			setCat("messages-4","msg");
			setCat("monitor-message","msg");
			
			setCat("inbox-1","inbox");
			setCat("inbox-2","inbox");
			setCat("inbox-3","inbox");
			setCat("inbox-4","inbox");
			setCat("inbox-in-1","inbox");
			setCat("inbox-in-2","inbox");
			setCat("inbox-notif","inbox");
			setCat("inbox-out-1","inbox");
			setCat("inbox-out-2","inbox");
			
			setCat("folder-1","folders");
			setCat("folder-2","folders");
			setCat("folder-add","folders");
			setCat("folder-cloud","folders");
			setCat("folder-connection","folders");
			setCat("folder-favorite","folders");
			setCat("folder-minus","folders");
			setCat("folder-open","folders");
			setCat("folder-x","folders");
			
			setCat("search-normal-1","search");
			setCat("search-normal-2","search");
			setCat("search-favorite-1","search");
			setCat("search-favorite-2","search");
			setCat("search-status-1","search");
			setCat("search-status-2","search");
			setCat("search-zoom-in","search");
			setCat("search-zoom-in-2","search");
			setCat("search-zoom-out-1","search");
			setCat("search-zoom-out-2","search");
			
			setCat("toggle-on-round","toggle");
			setCat("toggle-on-square","toggle");
			setCat("toggle-off-round","toggle");
			setCat("toggle-off-square","toggle");
			
			setCat("heart","hearts");
			setCat("heart-add","hearts");
			setCat("heart-circle","hearts");
			setCat("heart-edit","hearts");
			setCat("heart-search","hearts");
			setCat("heart-slash","hearts");
			setCat("heart-tag","hearts");
			setCat("heart-tick","hearts");
			setCat("heart-x","hearts");
			setCat("hearts","hearts");
			setCat("document-favorite","hearts");
			setCat("document-favorite-1","hearts");
			setCat("folder-favorite","hearts");
			setCat("message-dots-favorite","hearts");
			setCat("notif-favorite","hearts");
			setCat("picture-favorite","hearts");
			setCat("search-favorite-1","hearts");
			setCat("search-favorite-2","hearts");
			
			setCat("notif-circle","notif");
			setCat("notif-square","notif");
			setCat("notif-text-square","notif");
			setCat("notif-favorite","notif");
			setCat("cloud-notif","notif");
			setCat("inbox-notif","notif");
			setCat("mail-notif","notif");
			setCat("message-notif","notif");
			
			setCat("login","login");
			setCat("login-2","login");
			
			setCat("logout-1","logout");
			setCat("logout-2","logout");
			
			setCat("download-1","download");
			setCat("download-2","download");
			setCat("download-square","download");
			setCat("receive-diagonal-down","download");
			setCat("receive-diagonal-square","download");
			setCat("document-download","download");
			setCat("picture-download","download");
			
			setCat("upload-1","upload");
			setCat("upload-2","upload");
			setCat("upload-square","upload");
			setCat("send-diagonal-up","upload");
			setCat("send-diagonal-square","upload");
			setCat("document-upload","upload");
			setCat("picture-upload","upload");
			
			setCat("bell-1","objects");
			setCat("bell-2","objects");
			setCat("bell-3","objects");
			setCat("birdhouse","objects");
			setCat("broom","objects");
			setCat("bulb","objects");
			setCat("calculator","objects");
			setCat("crown-1","objects");
			setCat("crown-2","objects");
			setCat("diamonds","objects");
			setCat("eraser","objects");
			setCat("eraser-square","objects");
			setCat("flag-1","objects");
			setCat("flag-2","objects");
			setCat("flask","objects");
			setCat("gift","objects");
			setCat("glasses","objects");
			setCat("judge","objects");
			setCat("key","objects");
			setCat("key-square","objects");
			setCat("lamp-1","objects");
			setCat("lamp-2","objects");
			setCat("lifebuoy","objects");
			setCat("lock-1","objects");
			setCat("lock-2","objects");
			setCat("lock-circle","objects");
			setCat("magic-star","objects");
			setCat("magic-wand","objects");
			setCat("mail","objects");
			setCat("mirror","objects");
			setCat("nut","objects");
			setCat("plug","objects");
			setCat("ruler","objects");
			setCat("ruler-and-pen","objects");
			setCat("scissors","objects");
			setCat("scissors-square","objects");
			setCat("shield","objects");
			setCat("telescope","objects");
			setCat("ticket-1","objects");
			setCat("ticket-2","objects");
			setCat("trash","objects");
			setCat("trash-square","objects");
			setCat("trophy","objects");
			
			setCat("chrome","logos");
			setCat("grammarly","logos");
			setCat("instagram","logos");
			
			setCat("bubbles","other");
			setCat("convert","other");
			setCat("fingerprint-circle","other");
			setCat("flash-1","other");
			setCat("flash-circle-1","other");
			setCat("flash-circle-2","other");
			setCat("flash-slash","other");
			setCat("flash-speed","other");
			setCat("ghost","other");
			setCat("level","other");
			setCat("math-1","other");
			setCat("math-2","other");
			setCat("share","other");
			setCat("status","other");
			setCat("story","other");
			setCat("teacher","other");
			setCat("triangle","other");
			setCat("pet","other");
			setCat("omega-circle","other");
			setCat("omega-square","other");
			
			setCat("aquarius","constellation");
			setCat("gemini","constellation");
			setCat("gemini-2","constellation");			
			setCat("sagittarius","constellation");
			
			/*----------------------*/
			
			// find anything untagged
			/*
			let argh = [];
			
			document.querySelectorAll(`.icon-each[icon-name]:not([icon-name=""])`)?.forEach(icon => {
				argh.push(icon.getAttribute("icon-name"))
			})
			
			let elementCount = new Map();
			argh.forEach(el => {
				elementCount.set(el, (elementCount.get(el) || 0) + 1);
			});

			const www = argh.filter(el => elementCount.get(el) === 1);
			console.log(www)
			*/
		},0)
		
	}
})
.catch(err => console.error(err));
